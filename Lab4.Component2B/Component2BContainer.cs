﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Contract;

namespace Lab4.Component2B
{
    public class Component2BContainer : AbstractComponent, Interface1
    {
        public Component2BContainer()
        {
            this.RegisterProvidedInterface(typeof(Interface1), this);
        }
        
        public override void InjectInterface(Type type, object impl)
        {
            throw new NotImplementedException();
        }

        public void MetodaA()
        {
            Console.WriteLine("MetodaA - komponent 2B");
        }

        public void MetodaB()
        {
            Console.WriteLine("MetodaB - komponent 2B");
        }
    }
}
