﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 


using Lab4.Component1;
using ComponentFramework;
using Lab4.Component2B;
using Lab4.Contract;
namespace Lab4.MainB
{
    class Program
    {
        static void Main(string[] args)
        {
            IContainer kontener = new Container();
            Component1Container kontenerKlasa1 = new Component1Container();
            Component2BContainer kontenerklasa2 = new Component2BContainer();

            kontener.RegisterComponent(kontenerKlasa1);
            kontener.RegisterComponent(kontenerklasa2);

            kontener.GetInterface<Interface1>().MetodaA();
            Console.ReadKey();
        }
    }
}
