﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Contract;

namespace Lab4.Component1
{
    public class Component1Container :AbstractComponent
    {
        Interface1 interfaceA;

        public Component1Container()
        {
            this.RegisterRequiredInterface(typeof(Interface1));
        }

        public override void InjectInterface(Type type, object impl)
        {
            if (typeof(Interface1) == type)
            {
                interfaceA = (Interface1)impl;
            }
        }
    }
}
